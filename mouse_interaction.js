// when mouse is pressed return the tile object that was clicked if you clicked in the canvas
function mousePressed() {
    //make sure it is on canvas
    game.drawMap = 1;
    let mX = mouseX;
    let mY = mouseY;
    if (mX <= 0 || mX >= game.canvasWidth || mY <= 0 || mY >= game.canvasHeight) {
    }
    else {
        // if in movement mode

        if (game.moveMode == 1) {
           game.secondaryTile = findTile(mX, mY);
           if (canMoveHere(game.primaryTile, game.secondaryTile, game.numberOfTilesPerRow, game.tileArray.length)) {
                if (canUnitEnterTile(game.primaryTile.units, game.secondaryTile.type)) {
                    // if the move to tile has a unit in it
                    if (game.secondaryTile.isUnitsDefined()) {
                        // if the unit is friendly
                        if (game.secondaryTile.units.playerOwner == game.playerWhosTurnItIs) {
                            cantMoveHereAlert();
                        }
                        // if the unit is an enemy
                        else {
                            // calculate battle, and return if the attacker destroyed the defender as true or flase
                            if (battleOutcome(game.primaryTile.units, game.secondaryTile.units)) {
                                moveUnitToTile(game.primaryTile, game.secondaryTile, game.primaryTile.units);
                                game.moveMode = 0; 
                                removeButtonsFromPage();
                            }
                            // if the attacker lost
                            else {
                                enemyNotDefeated();
                                game.primaryTile.units.movesMadeThisTurn++;
                                game.moveMode = 0; 
                                removeButtonsFromPage();
                            }
                            // if the attacker has no health left. remove it
                            if (game.secondaryTile.units.health < 1) {
                                game.secondaryTile.units = null;
                                attackerWasKilledAlert();
                            }
                        }
                    }
                    else {
                        moveUnitToTile(game.primaryTile, game.secondaryTile, game.primaryTile.units);
                        game.moveMode = 0;           
                        removeButtonsFromPage();
                    }                    
                }
                else {
                    cantMoveHereAlert();
                }
            }
            else {
                cantMoveHereAlert();
            }
        }
        // if not in move mode, then regular tile click
        else {
            findTile(mX, mY).clicked();
        }
    }
}



// Fnds and returns a tile based on x, y coordinates in the canvas.
function findTile(x, y) {



    // get the x and y. if less than 100, then number 0. 
    // else get first digit.
    // find the array object at point x digit + (y digit * 10)

    let tileXStart = Math.floor(x/game.tileSize)*game.tileSize;
    let tileYStart = Math.floor(y/game.tileSize)*game.tileSize;



    for (i = 0; i < game.tileArray.length; i++) {
        if (tileXStart == game.tileArray[i].xStart && tileYStart == game.tileArray[i].yStart) {
            return game.tileArray[i];
        }
    }
    return null;
}

function setPrimarySecondaryTiles(prim) {
    game.secondaryTile = game.primaryTile;
    game.primaryTile = prim;


}