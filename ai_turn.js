/**
 * 
 * Author: Robert D Clark
 * 
 * This is the script that handles ai_turns
 * 
 *  AI Turn Phase
 *   // Try building some random improvements
     // Then try building some random units
     // Then try moving units in a random direction (including battling)
     // Then Try building random improvements this time using all the remaining money
     // Pass turn
 * 
 * 
 * 
 */

 function aiTurn() {
    tryBuildingRandomImprovements();
    tryBuildingRandomUnits();
    tryMovingAllUnitsRandomly();
    tryBuildingImprovementsAndUnitsAllMoney();
 }

 function tryBuildingRandomImprovements() {
    let tilesWithAiUnits = tileWithAiUnits();
    // look for all units under players control
    // choose them at random
    // check if the tile has an improvement
    //  if it doesn't, try an make a random improvement that the unit can make  
 }

 function tryBuildingRandomUnits() {
    let aiOwnedTiles = tileOwnedByAi();
    // look for all improvements that you own.
    // select them at random
    // try and make random unit that the improvement can make
 }

 function tryMovingAllUnitsRandomly() {
    // find all units that player owns
    // loop through the units
    // loop a random number between the units number of moves and 0
    //      move the units to a random adjacent tile that it can move on
    let tilesWithAiUnits = tileWithAiUnits();
    for (i = 0; i < tilesWithAiUnits.length; i++) {
        let randNumMove = int(random(1, tilesWithAiUnits[i].units.move));
            aiMoveUnit(tilesWithAiUnits[i]);
    }
 }

 function tryBuildingImprovementsAndUnitsAllMoney() {
    tryBuildingRandomImprovements();
    tryBuildingRandomUnits();
 }

 function aiMoveUnit(aiTileWithUnit) {
    let hasntMoved = true;
    let tilesThatExist = whichTilesAroundATileExist(aiTileWithUnit)
    // check if that direction is valid
    while (hasntMoved) {
        // choose a random direction
        let randDirection = int(random(1,8));
        if (tilesThatExist.includes(randDirection)) {
            // find the tile in that direction
            let aiTileToMoveTo = tileInThatDirection(randDirection, aiTileWithUnit);
            // check if the unit can move onto that tile type
            if (aiTileWithUnit.units.canMoveOn.includes(aiTileToMoveTo.type)) {
                // if the tile is empty, move to it, delete the old one
                // if not, check if its a friendly in the square
                if (aiTileToMoveTo.isUnitsDefined() && aiTileToMoveTo.units.playerOwner !== 0) {
                    // do battle then move unit if battle succeeded
                    if (battleOutcome(aiTileWithUnit.units, aiTileToMoveTo.units)) {
                        aiTileToMoveTo.units = aiTileWithUnit.units;
                        aiTileWithUnit.units = null;
                        // only finnish if it ends up moving the unit
                        console.log('Battle Won');
                        // if the attacker has no health left. remove it
                        if (aiTileToMoveTo.units.health < 1) {
                            aiTileToMoveTo.units = null;
                            attackerWasKilledAlert();
                        }
                    }
                    else {
                        if (aiTileWithUnit.units.health < 1) {
                            aiTileWithUnit.units = null;
                            attackerWasKilledAlert();
                        }
                        console.log('Battle lost');
                    }

                    hasntMoved = false;
                }
                else {
                    aiTileToMoveTo.units = aiTileWithUnit.units;
                    aiTileWithUnit.units = null;
                    // only finnish if it ends up moving the unit
                    hasntMoved = false;
                }
            }
        }
    }   
 }

function tileInThatDirection(direction, currentTile) {
    let newTile;
    let newTileNum;

    console.log("Direction of movement: " + direction);
    // find the new tile
    switch(direction) {
        case 1:
            newTile = game.tileArray[(currentTile.tileNumber - game.numberOfTilesPerRow) - 2];
            return newTile;
        case 2:
            newTile = game.tileArray[currentTile.tileNumber - game.numberOfTilesPerRow - 1];
            return newTile;
        case 3:
            newTile = game.tileArray[(currentTile.tileNumber - game.numberOfTilesPerRow)];
            return newTile;
        case 4:
            newTileNum = currentTile.tileNumber - 2;
            newTile = game.tileArray[newTileNum];
            return newTile;
        case 5:
            newTile = game.tileArray[currentTile.tileNumber];
            return newTile;
        case 6:
            newTile = game.tileArray[(currentTile.tileNumber + game.numberOfTilesPerRow) - 2];
            return newTile;
        case 7:
            newTile = game.tileArray[currentTile.tileNumber + game.numberOfTilesPerRow - 1];
            return newTile;
        case 8:
            newTile = game.tileArray[(currentTile.tileNumber + game.numberOfTilesPerRow)];
            return newTile;
    }
}

 function tileOwnedByAi() {
    let tArrayOwned = [];
    for (i = 0; i < game.tileArray.length; i++) {
        if (game.tileArray[i].tilePlayerOwner == 0) {
            tArrayOwned.push(game.tileArray[i]);
        }
    }
    return tArrayOwned;
 }

 function tileWithAiUnits() {
    let tArrayUnits = [];
    for (i = 0; i < game.tileArray.length; i++) {
        if (game.tileArray[i].isUnitsDefined()) {
            if (game.tileArray[i].units.playerOwner == game.playerWhosTurnItIs) {
                tArrayUnits.push(game.tileArray[i]);
            }
        }
    }
    return tArrayUnits;
 }