
// The ordering of turns

function startNextTurn() {
    // if there are more players, increment by 1 player
    if (game.playerWhosTurnItIs < game.numberOfPlayers) {
        game.playerWhosTurnItIs++;
    }
    else {
        game.playerWhosTurnItIs = 0;
    }

    let player = game.players[game.playerWhosTurnItIs];

    checkForTurnWinner();
    checkForScoreWinner();

    if (player.alive) {
        if (player.numCities == 0 && player.turn > 1) {
            playerHasLost(player);
            startNextTurn();
        }
        if (player.playerNumber == 0) {
            aiTurn(game);
            endTurnButtonPressed();
        }
    }
    else {
        startNextTurn();
    }
}

function endTurnButtonPressed() {
    // turn number and gold go up by 1 for the players whos turn it was
    let player = game.players[game.playerWhosTurnItIs];

    player.turn++;
    player.playerGold++;
    player.score += 100;

    // search for all improvements, and increase gold per values.
    for (i = 0; i < game.tileArray.length; i++) {
        if (player.playerNumber == game.tileArray[i].tilePlayerOwner && game.tileArray[i].isImprovementDefined()) {
            player.playerGold += game.tileArray[i].improvement.giveGPerTurn;
            player.score += game.tileArray[i].improvement.giveScorePerTurn;
        }
        if (game.tileArray[i].isUnitsDefined()) {
            game.tileArray[i].units.resetMovesMadeForTurn();
        }
    }

    
    startNextTurn();

}

