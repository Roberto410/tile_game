
/* Author: Robert D Clak
*
*
* Functioning map sizes:
*   width 1600; height 1000; numRows 40; = 1000 tiles
*   width 1600; height 1000; numRows 16; = 160 tiles
*   width 2000; height 1000; numRows 20; = 200 tiles
*   width 1000; height 1000; numRows 10; = 100 tiles
*   width 1000; height 1000; numRows 20; = 400 tiles     
*   width 2000; height 2000; numRows 20; = 400 tiles 
*   width 4000; height 4000; numRows 40; = 1600 tiles
*
*   GameState() initialises the game state. it takes 4 parameters in order:
*       Canvas Width, Canvas Height,
*       Number of Tiles in a Row,
*       Perlin Noise scale for map generation,
*       Starting amount of gold,
*       Number of Players
*       turn Limit
*       Score limit
* 
*   Max Supported Players 5
*
*   Small 100 tile 2 player  default settings = GameState(1000, 1000, 10, 0.1, 6, 2, 20, 6000) 
*   Medium 160 tile 2 player default settings = GameState(1600, 1000, 16, 0.1, 6, 2, 30, 10000) 
*   Large 1000 tile 2 player default settings = GameState(1600, 1000, 40, 0.08, 6, 2, 30, 10000) 
*   Super Large tile 5 player default settingsGameState(6000, 3000, 40, 0.07, 6, 5, 30, 10000);
*/

var game;
var vCanvas;

function preload() {

    grassTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/grasstile.png');
    waterTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/watertile.png');
    deepWaterTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/deepWaterTile.png');
    sandTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/sandTile.png');
    forestTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/darkGrass.png');
    mountainTileImg = loadImage('https://otherresources.s3-ap-southeast-2.amazonaws.com/mountainTile.png');
    gameSoundtrack = loadSound('https://typebeatpackages.s3-ap-southeast-2.amazonaws.com/miscMusic/Bass_Master-128k.mp3');
}



function setup() {

    game = new GameState(1000, 1000, 10, 0.1, 5, 1, 20, 5000);
    createP("Turn: ").addClass('turnText');
    createP("Tile Info: ").addClass('tileInfo');
    endturnButton = createButton("End Turn").addClass('turnButton');
    endturnButton.position(game.canvasWidth + 20, game.canvasHeight);
    endturnButton.mousePressed(endTurnButtonPressed);
    turnCountElement = select('.turnText');
    golCountElement = select('.gold');
    turnButtonElement = select('.turnButton');
    tileInfoElement = select('.tileInfo');
    vCanvas = createCanvas(game.canvasWidth, game.canvasHeight);
    vCanvas.parent('gameArea');

    
    gameSoundtrack.loop();
    gameSoundtrack.setVolume(0.5);
    // Map datasheet
    /*
     tileMap = [
        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,
        0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,
        0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,1,0,0,0,
        0,0,0,0,0,1,1,0,0,1,0,0,0,0,0,1,1,0,0,0,
        0,0,0,0,1,1,0,0,0,1,1,1,1,0,1,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,
        0,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    ] */
    

    // If there isn't a specified tile map then it generates a tile map, using the product of the number of rows and columns
    if (typeof tileMap === 'undefined' || tileMap === null) {
        tileMap = generateTileMap(game.numberOfTilesPerRow, game.numberOfTilesPerColumn, game.percentLand);
    }

    let tileX = 0;
    let tileY = 0;

// Creating the tile map.
    for (i = 0; i < game.numberOfTilesPerColumn * game.numberOfTilesPerRow; i++) {
        game.tileArray[i] = new Tile(tileX,tileY,tileMap[i], game.tileSize, i+1);

        if (tileX < game.canvasWidth - game.tileSize) {
            tileX += game.tileSize;
        }
        else {
            tileX = 0;
            tileY += game.tileSize;
        }
    }

    // Set the starting cities and builder and swordsman
    for (i = 0; i < game.players.length; i++) {
        let chosenRandomCity = false;
        while(!chosenRandomCity) {
            let ranTile = random(game.tileArray);
            if (ranTile.type == 0) {
                ranTile.createImprovement(0, game);
                ranTile.createUnits(2, game);
                game.players[game.playerWhosTurnItIs].numCities++;
                ranTile.units.movesMadeThisTurn = 0;
                chosenRandomCity = true;
                if (game.players.length - 1 > game.playerWhosTurnItIs) {
                    game.playerWhosTurnItIs++;
                }
            }
        }
    }
    // reset the player whose turn it is.
    game.playerWhosTurnItIs = 1;
   

    // set primary and secondary tile
    game.primaryTile = game.tileArray[0];
    game.secondayTile = game.tileArray[0];

}




// the draw function, it's called every frame
function draw() {
    //background(255);
    if (game.drawMap == 1){
        drawTheMap();
        game.drawMap = 0;
    }
}

function drawTheMap() {
    //Draws the map
    background(255);
    for (i = 0; i < game.tileArray.length; i++){
        game.tileArray[i].show();
    }

    let tile = game.primaryTile;
    let player = game.players[game.playerWhosTurnItIs];
    let playerText = player.playerText;
    
    let turnText = playerText + " " + player.playerColorText +
        " | Turn Number: " + player.turn + 
        " | Score: " + player.score + 
        " | Gold: " + player.playerGold; 

    let tileInfoText = "Tile Type " + tile.tileTypeText;

    if (tile.isImprovementDefined()) {
        tileInfoText = tileInfoText.concat(
            " | Improvement: " + tile.improvement.name +
            " - Gives " + tile.improvement.giveGPerTurn + " Gold" +
            " - Gives " + tile.improvement.giveScorePerTurn + " Score");
    }
    if (tile.isUnitsDefined()) {
        tileInfoText = tileInfoText.concat(
            " | Unit: " + tile.units.name +
            " - Health: " + tile.units.health +
            " - Attack: " + tile.units.attack +
            " - Defence " + tile.units.defence + 
            " - Moves: " + tile.units.move);
    }

    turnCountElement.html(turnText);
    tileInfoElement.html("Tile Info: " + tileInfoText);
}


function battleOutcome(attacker, defender) {
    // calculate battle, and return if the attacker destroyed the defender as true or flase

    // both attackers and defenders may get hurt. defender always takes 1 damage

    // the attackers att minus the defenders defence gives damage delt


    damageDeltToDefender = attacker.attack - defender.defence;
    damageDeltToAttacker = defender.attack - attacker.defence;
    // if damage is 
    defender.health -= 1;



    if (damageDeltToDefender > 0) {
        defender.health -= damageDeltToDefender;
    }
    if (damageDeltToAttacker > 0) {
        attacker.health -= damageDeltToAttacker;
    }

    if (defender.health < 1) {
        return true;
    }
    else {
        return false;
    }
}


function canYouBuyThis(playerGold, buyCost) {
    let cost = playerGold - buyCost;
    if (cost < 0) {
        return false;
    }
    else {
        return true;
    }
}



function updateGoldPurchase(gCost) {
    game.players[game.playerWhosTurnItIs].playerGold -= gCost;
}











