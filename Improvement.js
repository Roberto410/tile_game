class Improvement {
    constructor(type) {

        this.type = type;
        switch (type) {
            case 0:
                this.name = "City";
                this.tileColor = color(0,0,0);
                this.gCost = 5;
                this.giveGPerTurn = 2;
                this.giveScorePerTurn = 100;
                break;
            case 1:
                this.name = "Fortress";
                this.tileColor = color(255,30,180);
                this.gCost = 2;
                this.giveGPerTurn = 1;
                this.giveScorePerTurn = 10;
                break;
            case 2:
                this.name = "Market";
                this.tileColor = color(128,0,128);
                this.gCost = 3;
                this.giveGPerTurn = 4;
                this.giveScorePerTurn = 50;
                break;
        }
    }


    show(x, y, s) {
        fill(this.tileColor);
        square(x, y, s * 0.6);
    }

}
// Create improvement functions

function createCity() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 5)) {
        game.primaryTile.createImprovement(0, game);
        updateGoldPurchase(game.primaryTile.improvement.gCost);
        game.players[game.playerWhosTurnItIs].numCities++;
    }
    else {
       noGoldAlert();
    }
    removeButtonsFromPage();
}

function createFortress() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 2)) {
        game.primaryTile.createImprovement(1, game);
        updateGoldPurchase(game.primaryTile.improvement.gCost);
    }
    else {
       noGoldAlert();
    }
    removeButtonsFromPage();
}

function createMarket() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 3)) {
        game.primaryTile.createImprovement(2, game);
        updateGoldPurchase(game.primaryTile.improvement.gCost);
    }
    else {
       noGoldAlert();
    }
    removeButtonsFromPage();
}

function removeImprovement() {
    let tile = game.primaryTile;
    // if improvement was a city, then num cities --
    if (tile.improvement.type == 0) {
        game.players[tile.tilePlayerOwner].numCities--;
    }
    tile.improvement = null;
    tile.tilePlayerOwner = 0;
    removeButtonsFromPage();
}



