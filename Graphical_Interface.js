// Button Definitions
// Create and Remove Buttons



function createUiButtons(tile) {
    removeButtonsFromPage();

    tile.possibleImprovements = [];
    tile.possibleUnits = [];
  // if there is an improvement
    if (tile.isImprovementDefined()) {
        // If player owns tile
        createRemoveImprovementButton(tile);
    }
    // if you do own this tile
    if (tile.tilePlayerOwner == game.playerWhosTurnItIs || tile.tilePlayerOwner == null) {
        // if no unit is defined in the tile and there is an improvement on the tile
        if (!tile.isUnitsDefined() && tile.isImprovementDefined()) {
            createUnitButtons(tile); 
        }
        // if a unit exists in the tile
        else if (!tile.isImprovementDefined()) {
            setPossibleImprovements(tile);
        }
        drawImprovementButtons(tile);
    }
    // if a unit is in the tile
    if (tile.isUnitsDefined()) {
        // if unit is owned by player
        if (tile.units.playerOwner == game.playerWhosTurnItIs) {
            if (tile.units.hasMovesRemaining()) {
                // create button that allows movement of the unit in the tile
                buttonMoveUnit = createButton("Move Unit");
                buttonMoveUnit.position(game.canvasWidth + 20, game.canvasHeight * 0.7);
                buttonMoveUnit.mousePressed(moveUnitButtonPressed);
                // after movement is finished, remove the move button
            }
        }
        // if there are moves remaining
    }
}

function setPossibleImprovements(tile) {
    // there is a unit on the square, and the player owns it, then you can build
    if (tile.isUnitsDefined() && tile.units.playerOwner == game.playerWhosTurnItIs) {
        // if improvement isn't defined, look at avaliable improvements for the tile, based on type, and player avaliable improvements
            switch (tile.type) {
                case 0: 
                    tile.possibleImprovements.push(0, 1);
                    break;
                case 1:
                    //nothing
                    break;  
                case 2:
                    tile.possibleImprovements.push(1, 2);
                    break;
                case 3:
                    tile.possibleImprovements.push(0, 1);
                    break;
                case 4:
                    tile.possibleImprovements.push(1);
                    break;
                case 5:
                    //nothing
                    break;
            }
        }
        // if the player has no cities
        else if (game.players[game.playerWhosTurnItIs].numCities == 0) {
            tile.possibleImprovements.push(0);
        }
}

function drawImprovementButtons(tile) {
    // Create buttons for the possible improvements
    for (i = 0; i < tile.possibleImprovements.length; i++) {
        switch (tile.possibleImprovements[i]) {
            case 0:
                button0 = createButton("Create City: 5 Gold");
                button0.position(game.canvasWidth + 20, game.canvasHeight * 0.15);
                button0.mousePressed(createCity);
                break;
            case 1:
                button1 = createButton("Create Fortress: 2 Gold");
                button1.position(game.canvasWidth + 20, game.canvasHeight * 0.2);
                button1.mousePressed(createFortress);
                break;
            case 2:
                button2 = createButton("Create Market: 3 Gold");
                button2.position(game.canvasWidth + 20, game.canvasHeight * 0.25);
                button2.mousePressed(createMarket);
                break;
        }
    }
}



function createUnitButtons(tile) {
    // set units that can be made depending on tile improvements 
    switch (tile.improvement.type) {
        case 0:
            tile.possibleUnits.push(0,2);
            break;
        case 1:
            tile.possibleUnits.push(0, 1);
            break;
        case 2:
            // nothing
            break;
    }

    // search through list of possible units
    for (i = 0; i < tile.possibleUnits.length; i++) {
        switch (tile.possibleUnits[i]) {
            case 0:
                buttonUnit0 = createButton("Create Swordsman: 1 Gold");
                buttonUnit0.position(game.canvasWidth + 20, game.canvasHeight * 0.8);
                buttonUnit0.mousePressed(createSwordsman);
                break;
            case 1:
                buttonUnit1 = createButton("Create Sheildman: 2 Gold");
                buttonUnit1.position(game.canvasWidth + 20, game.canvasHeight * 0.75);
                buttonUnit1.mousePressed(createSheildman);
                break;
            case 2:
                buttonUnit2 = createButton("Create Builder: 1 Gold");
                buttonUnit2.position(game.canvasWidth + 20, game.canvasHeight * 0.7);
                buttonUnit2.mousePressed(createBuilder);
                break;
        }
    }
}


function createRemoveImprovementButton(tile) {
    if (tile.tilePlayerOwner == game.playerWhosTurnItIs) {
        buttonImprovement = createButton('Remove ' + tile.improvement.name);
        buttonImprovement.position(game.canvasWidth + 20, game.canvasHeight * 0.1);
        buttonImprovement.mousePressed(removeImprovement);
        tile.possibleImprovements = [];
    }
    else {
        // if a unit is defined
        if (tile.isUnitsDefined()) {
            // if the unit belongs to the player
            if (tile.units.playerOwner == game.playerWhosTurnItIs) {
                buttonImprovement = createButton('Remove ' + tile.improvement.name);
                buttonImprovement.position(game.canvasWidth + 20, game.canvasHeight * 0.1);
                buttonImprovement.mousePressed(removeImprovement);
                tile.possibleImprovements = [];
            }
        }
    }
}

function removeButtonsFromPage() {
    if (typeof buttonImprovement !== 'undefined') {
        buttonImprovement.remove();
    }
    if (typeof button0 !== 'undefined') {
        button0.remove();
    }
    if (typeof button1 !== 'undefined') {
        button1.remove();
    }
    if (typeof button2 !== 'undefined') {
        button2.remove();
    }
    if (typeof buttonUnit0 !== 'undefined') {
        buttonUnit0.remove();
    }
    if (typeof buttonUnit1 !== 'undefined') {
        buttonUnit1.remove();
    }
    if (typeof buttonUnit2 !== 'undefined') {
        buttonUnit2.remove();
    }
    if (typeof buttonMoveUnit !== 'undefined') {
        buttonMoveUnit.remove();
    }
}
