class Units {
    constructor(type, player) {
        this.playerOwner = player;

        this.type = type;
        switch (type) {
            case 0:
                this.name = "Swordsman";
                this.unitColor = color(255,10,10);
                this.gCost = 1;
                this.attack = 3;
                this.defence = 2;
                this.health = 2;
                this.move = 2;
                this.range = 1;
                this.canMoveOn = [0, 1, 2, 3];
                break;
            case 1:
                this.name = "Sheildman";
                this.unitColor = color(200,200,200);
                this.gCost = 2;
                this.attack = 1;
                this.defence = 4;
                this.health = 3;
                this.move = 1;
                this.range = 1;
                this.canMoveOn = [0, 2, 3, 4];
                break;
            case 2:
                this.name = "Builder";
                this.unitColor = color(153,204,255);
                this.gCost = 1;
                this.attack = 1;
                this.defence = 1;
                this.health = 2;
                this.move = 3;
                this.range = 1;
                this.canMoveOn = [0,2,3,4];
        }
        this.movesMadeThisTurn = this.move;
    }


    show(x, y, s) {
        let xPos = x + s * 0.7;
        let yPos = y + s * 0.7;

        fill(this.unitColor);
        ellipse(xPos, yPos, s * 0.5);

        // if a player owns this tile, set tile to player color
        let color = game.players[this.playerOwner].color;
        fill(color);
        ellipse(xPos, yPos, s * 0.2);
    }

    hasMovesRemaining() {
        if (this.movesMadeThisTurn < this.move) {
            return true;
        }
        else {
            return false;
        }
    }

    resetMovesMadeForTurn() {
        this.movesMadeThisTurn = 0;
    }

    increaseMovesMade() {
        this.movesMadeThisTurn++;
    }
}

function moveUnitButtonPressed() {
    // set the GameState to be waiting for a new square move input
    if (game.moveMode == 0) {
        setGameStateToMovement();
    }
    else {
        game.moveMode = 0;
    }
}

function createSwordsman() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 1)) {
        game.primaryTile.createUnits(0, game.playerWhosTurnItIs);
        updateGoldPurchase(game.primaryTile.units.gCost);
    }
    else {
        noGoldAlert();
    }
    removeButtonsFromPage();
}

function createSheildman() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 2)) {
        game.primaryTile.createUnits(1, game.playerWhosTurnItIs);
        updateGoldPurchase(game.primaryTile.units.gCost);
    }
    else {
        noGoldAlert();
    }
    removeButtonsFromPage();
}

function createBuilder() {
    if (canYouBuyThis(game.players[game.playerWhosTurnItIs].playerGold, 1)) {
        game.primaryTile.createUnits(2, game.playerWhosTurnItIs);
        updateGoldPurchase(game.primaryTile.units.gCost);
    }
    else {
        noGoldAlert();
    }
    removeButtonsFromPage();
}
