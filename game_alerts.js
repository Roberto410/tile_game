/*
*
* All the alerts as callable functions
*
*/


function winnerAlert(player) {
    alert(player.playerText + " Is The Winner - With a Score of: " + player.score);
}

function noGoldAlert() {
    alert("You do not have enough Gold.");
}

function cantMoveHereAlert() {
    alert("You Cannot Move Here");
}

function enemyNotDefeated() {
    alert("The Attacker did not Defeate the Enemy");
}

function attackerWasKilledAlert() {
alert("The Attacker Was Killed");
}