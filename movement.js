/*
* Author: Robert D Clak
*
*
* Movement functions for the tile game
*
*/


function setGameStateToMovement() {
    game.moveMode = 1;
}

function canUnitEnterTile(unit, type) {
    for (i = 0; i < unit.canMoveOn.length; i++) {
        if (unit.canMoveOn[i] == type) {
            return true;
        }
    }
    return false;
}

function moveUnitToTile(MprimTile, MsecTile, unit) {
    MsecTile.units = unit;
    MprimTile.units = null;
    MsecTile.units.movesMadeThisTurn++;
    // primTile.units. stop movements when all are done until next turn
}

function whichTilesAroundATileExist(tile) {
    let listOfExistingTile = [];
    
    let canMoveLeft = false;
    let canMoveRight = false;
    let canMoveUp = false;
    let canMoveDown = false;

    // Check and set wich directions can be moved
    if (tile.tileNumber % game.numberOfTilesPerRow !== 1) {
        canMoveLeft = true;
        listOfExistingTile.push(4);
    }
    if (tile.tileNumber % game.numberOfTilesPerRow !== 0) {
        canMoveRight = true;
        listOfExistingTile.push(5);
    }
    if (tile.tileNumber > game.numberOfTilesPerRow) {
        canMoveUp = true;
        listOfExistingTile.push(2);
    }
    if (tile.tileNumber < (game.tileArray.length - game.numberOfTilesPerRow)) {
        canMoveDown = true;
        listOfExistingTile.push(7);
    }    
    if (canMoveUp && canMoveRight) {
        listOfExistingTile.push(3);
    }

    if (canMoveUp && canMoveLeft) {
        listOfExistingTile.push(1);     
    }

    if (canMoveDown && canMoveRight) {
        listOfExistingTile.push(8);       
    }

    if (canMoveDown && canMoveLeft) {
        listOfExistingTile.push(6);   
    }

    console.log("Directions That the Ai can move" + listOfExistingTile);
    return listOfExistingTile;
}

// returns true or false depending if you can move there
function canMoveHere(primTile, secTile, tilesPerRow, numberOfTiles) {
    // check if secTile is within 1 tile of primTile
    let primTileNum = primTile.tileNumber;
    let secTileNum = secTile.tileNumber;
    let canMoveLeft = false;
    let canMoveRight = false;
    let canMoveUp = false;
    let canMoveDown = false;

    // Check and set wich directions can be moved
    if (primTileNum % tilesPerRow !== 0) {
        canMoveRight = true;
    }
    if (primTileNum % tilesPerRow !== 1) {
        canMoveLeft = true;
    }
    if (primTileNum > tilesPerRow) {
        canMoveUp = true;
    }
    if (primTileNum < (numberOfTiles - tilesPerRow)) {
        canMoveDown = true;
    }


    //Moving Diagonally
    if (canMoveUp && canMoveRight) {
        if (secTileNum == (primTileNum - tilesPerRow + 1)) {
            return true;
        }        
    }

    if (canMoveUp && canMoveLeft) {
        if (secTileNum == (primTileNum - tilesPerRow - 1)) {
            return true;
        }        
    }

    if (canMoveDown && canMoveRight) {
        if (secTileNum == (primTileNum + tilesPerRow + 1)) {
            return true;
        }        
    }

    if (canMoveDown && canMoveLeft) {
        if (secTileNum == (primTileNum + tilesPerRow - 1)) {
            return true;
        }        
    }


    // Moving Left or Right
    if (canMoveUp) {
        if (secTileNum == (primTileNum - tilesPerRow)) {
            return true;
        }        
    }

    if (canMoveDown) {
        if (secTileNum == (primTileNum + tilesPerRow)) {
            return true;
        }        
    }

    if (canMoveLeft) {
        if (secTileNum == (primTileNum - 1)) {
            return true;
        }        
    }

    if (canMoveRight) {
        if (secTileNum == (primTileNum + 1)) {
            return true;
        }        
    }

// if can't move anywhere return false
    return false;
}