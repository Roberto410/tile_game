// Generates a random tile map and returns an array of length numTiles
function generateTileMap(xTileLength, yTileLength, scl) {
    let tileMapArray = [];

        for (iY = 0; iY < yTileLength; iY++) {
            for (iX = 0; iX < xTileLength; iX++) {
                perlinNum = noise(iX * scl, iY * scl);
                if (perlinNum < 0.25) {
                    perlinNum = 4
                }
                else if (perlinNum < 0.38) {
                    perlinNum = 3
                }
                else if (perlinNum < 0.52) {
                    perlinNum = 0;
                }
                else if (perlinNum < 0.57) {
                    perlinNum = 2;
                }
                else if (perlinNum < 0.68) {
                    perlinNum = 1;
                }
                else if (perlinNum < 1) {
                    perlinNum = 5;
                }
                
                tileMapArray.push(perlinNum);
            }     
        } 

    return tileMapArray
}