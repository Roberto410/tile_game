class PlayerClass {
    constructor(pN, g) {

        let teamcolors = [
            color(200,200,200),
            color(255,255,0),
            color(0,255,255),
            color(255,0,0),
            color(255,0,255),
            color(100,100,100)];
        

        this.playerNumber = pN;
        this.playerText = "Player " + pN.toString();
        this.playerGold = g;
        this.turn = 1;
        this.score = 0;
        this.color = teamcolors[this.playerNumber];
        this.numCities = 0;
        this.alive = true;

        switch (this.playerNumber) {
            case 0:
                this.playerColorText = "Computer";
                break;
            case 1:
                this.playerColorText = "Yellow";
                break;
            case 2:
                this.playerColorText = "Blue";
                break;
            case 3:
                this.playerColorText = "Red";
                break;
            case 4:
                this.playerColorText = "Pink";
                break;
            case 5:
                this.playerColorText = "Grey";
                break;
        }
    }
}