
class GameState {
    constructor(cW, cH, nOTPR, pL, gld, numPlayers, tL, sL) {
    this.tileArray = [];
    this.canvasWidth = cW;
    this.canvasHeight = cH;
    this.numberOfTilesPerRow = nOTPR;
    this.tileSize = this.canvasWidth / this.numberOfTilesPerRow;
    this.numberOfTilesPerColumn = this.canvasHeight / this.tileSize;
    this.currentUserSelection = 0;
    this.primaryTile;
    this.secondayTile;
    this.percentLand = pL;
    this.gold = gld;
    this.turnNum = 0;
    this.moveMode = 0; // 0 movement mode off, 1 movement mode on
    this.numberOfPlayers = numPlayers; //num players not including computer
    this.turnLimit = tL;
    this.scoreLimit - sL;
    this.tileMap = [];
    this.drawMap = 1;



    this.players = []; // Player 0 will always be the computer
    for (let i = 0; i <= this.numberOfPlayers; i++) {
        this.player = new PlayerClass(i, gld);
        this.players.push(this.player);
        }
    this.playerWhosTurnItIs = 0;

    }
}