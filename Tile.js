
class Tile {
    constructor(x, y, t, s, tN) {
        // the color
        this.type = t;
        // the x and y of the bottom left corner of the tile
        this.xStart = x;
        this.yStart = y;
        this.size = s;
        this.possibleImprovements = [];
        this.tilePlayerOwner = null;
        this.units;
        this.tileNumber = tN;
       

        switch(this.type) {
            case 0:
                this.tileColor = color(30,255,100);
                this.tileTypeText = "Grass";
                this.tileImage = grassTileImg;
                break;
            case 1:
                this.tileColor = color(30,100,255);
                this.tileTypeText = "Water";
                this.tileImage = waterTileImg;
                break;
            case 2:
                this.tileColor = color(239,221,111);
                this.tileTypeText = "Sand";
                break;
            case 3:
                this.tileColor = color(1,50,32);
                this.tileTypeText = "Forest";
                break;
            case 4:
                this.tileColor = color(48,186,143);
                this.tileTypeText = "Mountain";
                break;
            case 5:
                this.tileColor = color(0,0,139);
                this.tileTypeText = "Ocean";
                break;
        }
    }


    show() {
        
        switch (this.type) {
            case 0: 
                try {
                    image(this.tileImage, this.xStart, this.yStart, this.size, this.size);
                }
                catch(err) {
                    this.makeSquareAndFill();
                }
                break;
            case 1:
                try {
                    image(waterTileImg, this.xStart, this.yStart, this.size, this.size)
                    
                }
                catch (err) {
                    this.makeSquareAndFill();
                }              
                break;
            case 2:
                try {
                    image(sandTileImg, this.xStart, this.yStart, this.size, this.size)
                    
                }
                catch (err) {
                    this.makeSquareAndFill();
                }              
                break;
            case 3:
                try {
                    image(forestTileImg, this.xStart, this.yStart, this.size, this.size)
                    
                }
                catch (err) {
                    this.makeSquareAndFill();
                }              
                break;
            case 4:
                try {
                    image(mountainTileImg, this.xStart, this.yStart, this.size, this.size)
                    
                }
                catch (err) {
                    this.makeSquareAndFill();
                }              
                break;
            case 5:
                try {
                    image(deepWaterTileImg, this.xStart, this.yStart, this.size, this.size)
                    
                }
                catch (err) {
                    this.makeSquareAndFill();
                }              
                break;
        } 
        //console.log("show tile");

        if (this == game.primaryTile) {
            fill(255, 255, 255, 100)
            square(this.xStart,this.yStart,this.size);
        }


        // only make improvement if there is one defined
        if (this.isImprovementDefined()) {
            this.improvement.show(this.xStart, this.yStart, this.size);
        }   

        if (this.isUnitsDefined()) {
            this.units.show(this.xStart, this.yStart, this.size)
        }

        // if a player owns this tile, set tile to player color
        if (this.tilePlayerOwner !== null) {
            let color = game.players[this.tilePlayerOwner].color;
            fill(color);
            square(this.xStart,this.yStart,this.size*0.2)
        }

    }


    // Creates a new improvement on this tile
    createImprovement(type, gameState) {
        this.improvement = new Improvement(type);
        this.tilePlayerOwner = gameState.playerWhosTurnItIs;
 
    }

    createUnits(type) {
        this.units = new Units(type, this.tilePlayerOwner);
    }


    clicked() {
        // Set this tile as the primary one, and the last one as the secondary
        setPrimarySecondaryTiles(this);
        createUiButtons(this);
 
    }

    //is an improvement defined for this tile?
    isImprovementDefined() {
        if (typeof this.improvement === 'undefined' || this.improvement === null) {
            return false;
        }
        else {
           return true;
        }
    }

    isUnitsDefined() {
        if (typeof this.units === 'undefined' || this.units === null) {
            return false;
        }
        else {
           return true;
        }
    }

    isWaterTile() {
        if (type == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    tileImprovementType() {
        return this.improvement.type;
    }

    makeSquareAndFill() {
        fill(this.tileColor);
        square(this.xStart,this.yStart,this.size);
    }
}

