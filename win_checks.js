


function playerHasLost(player) {
    alert(player.playerText + " Has Lost")
    player.alive = false;

    // if there is a winner
    if (isThereAWinner()) {
        let theWinner;
        for (i = 1; i < game.players.length; i++) {
            if(game.players[i].alive) {
                theWinner = game.players[i]
            }
        }
        winnerAlert(theWinner);
    }
}

function isThereAWinner() {
    let playersAlive = 0

    console.log(game.players[1].alive);
    for (i = 1; i < game.numberOfPlayers + 1; i++){
        console.log("1");
        if (game.players[i].alive) {
            playersAlive += 1;
            console.log("play alive: " + playersAlive);

        }

    }
    if (playersAlive <= 1) {
        console.log("3");
        return true;
    }
    return false;
}

function checkForScoreWinner() {
    for (i = 0; i < game.players.length; i++) {
        if (game.players[i].score > game.scoreLimit) {
            winnerAlert(game.players[i]);
        }
    }
}


function checkForTurnWinner() {
    let highestScoreingPlayer;
    let highestScore = 0;
    if (game.players[game.playerWhosTurnItIs].turn > game.turnLimit) {
        for (i = 0; i < game.players.length; i++){
            if (game.players[i].score > highestScore ) {
                highestScore = game.players[i].score;
                highestScoreingPlayer = i;
            }
        }
        winnerAlert(game.players[highestScoreingPlayer]);
    }
}