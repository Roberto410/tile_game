# Tile Game 1.0


* https://www.youtube.com/watch?v=txUvD5_ROIU


## things done
* make tile objects
* tiles will have a bunch of variables like their type, what unit is in it.
* make map based on the tiles type variable
* turn based. 
* every turn, gold goes up. 
* gold goes up based on improvement types
* score and every turn it goes up.
* unit objects. they are referenced in a tile, and fill it.
* unit have an att, def, team, range, move, health, lvl, xp.
* when unit tries to move into another creatures square that is an enemy, then battle
* improvement object. this might be a building, capital, 
* when you build improvements, gold should go down
* players have turns
* player stores turn count, gold, player number
* end of turn players score goes up
* when end turn is pressed, change to the next players turn (if 0, skip)
* units with player 0 are the computers
* if unit in the square, can't build unit.
* unit summoning sickness
* desert - tile that only markets can be built on
* market - inmporvement that generates gold and some score
* builder - first unit. builds cities and base improvements. low health defense

## things TODO
* add key press to activate end turn
* add key press to toggle move mode
* make building improvement and unit buttons a popup when you double press a tile
* inform you the next turn has happened
* Add images for all tiles
* Add images for Improvements
* Add images for units
* create AI player



## Game Modes

# Fortresses and cities.
1 player mode

* Score goes up
* once you run out of turns, then your score is your score


2 players mode

* small map size
* You make fortesses and cities. 
* you get gold each turn from your cities.
* your fortresses allow you to make basic swordsman.
* units can destroy enemy improvements.
* once all of your cities are destroyed, you loze.
* if you destroy all of an opponents city, you win.
* if time runs out, the winner has the highest score.




# How to Add things

## How to add tile types
* Open Tile.js
* In the constructor function, add the new tiles attributes to the switch statement
* 
* You need to define: 
*   this.tileColor = color(r,g,b);
*   this.tile.TypeText = "Grass"; or whatever
*   this.tileImage = imgVariableForTheTile; the image varible you set in the preload function in game.js
*   rebember to put break; at the end.
* 
* You need to add the square, and tile image to the show() function switch statement
* 
* Open map_generator.js
* add a new else if () statement to the chain and set the percentage number for that terain type at 
* some number less than 1 and more than the previous number
* 
* Open Graphical_Interface.js
* in setPossibleImprovements() function create a new case aregument for the new tile type, and push the improvement types that can be built on that tile type
* 
* Note: Units won't be able to move on the new tiles unless you say they can.
* 
* if you want an image for the tile you need to define it in the preload function in game.js


## How to Add improvments types
* Open Improvement.js
* In the constructor function, add the new improvement attributes to the switch statement
* 
* You need to define: 
*   this.tileColor = color(r,g,b);
*   this.tile.name = "city"; or whatever
*   this.gCost = cost to buy improvement;
*   this.giveGPerTurn = the amount of gold this gives per turn;
*   this.giveScorePerTurn = the amount of score it gives per turn;
*   rebember to put break; at the end.
*   
* You need to make a new function in the file called create'watever it's name is'()
*   - Example: function createFortress() {//whatever function does}; 
*   In the finctions first canYouBuyThis(); function call. Be sure to set the gold cost for buying the improvement. It is the last parameter.
* make sure to change the game.primaryTile.createImprovement(); first paramater. It sets the type of the improvement being created.
* 
* Open Graphical_Interface.js
* in setPossibleImprovement() push the improvement type to the array of possible improvements, based on a switch statment of what tile the set of improvements can be built on.
* 
* in the drawImprovementButtons() function, add a new switch case that creates the new button for making the improvement.
* name the buttons with increasing number. example: 'Case 2 has button2.'
* make sure you adjust it's y render position
* set the function that will be called when the button is clicked (eg. createMarket)
* 
* add a new if statement to the removeButtonsFromPage(); function for the new button
* 
* in the creatUnitButtons function create a new case aregument for the new improvement type, and push the units types that can be built on that improvement type

## How to Add Unit types
* Open Units.js
* In the constructor function, add the new unit attributes to the switch statement
* 
* You need to define:
*   this.name = "name of the thing";
*   this.unitColor = color(r,g,b);
*   this.gCost = cost to buy unit;
*   this.attack = units attack;
*   this.defence = units defence;
*   this.health = units health;
*   this.move = tunits movement;
*   this.range = units range;
*   this.canMoveOn = array of all the tile types is can move on example: '[0, 1, 2, 3]'
*   rebember to put break; at the end. 
* 
* make new function create'name of the new unit'() {}. this will be called when a button is pressed.
* In the finctions first canYouBuyThis(); function call. Be sure to set the gold cost for buying the unit. It is the last parameter.
* make sure to change the game.primaryTile.createUnit(); first paramater. It sets the type of the unit being created.
* 
* Open Graphical_Interface.js
* in createUnitButtons(); function add the unit to the improvement types that can create it.
* add a new switch case that creates the new button for making the unit.
* name the buttons with increasing number. example: 'Case 2 has buttonUnit2.'
* make sure you adjust it's y render position
* set the function that will be called when the button is clicked (eg. createSheildman)
* 
* add a new if statement to the removeButtonsFromPage(); function for the new button